var express = require("Express");
var router = express.Router();
const { check, validationResult } = require("express-validator"); //ทำการเช็คก่อนที่ส่งเข้าฐานข้อมูล

const MyBlogs = require("../models/myblogs");

router.get("/", function (req, res, next) {
  MyBlogs.GetAllMyBlogs(function (err, paramMyblogs) {
    if (err) throw err;

    res.render("blogs/index", {
      header: "ข้อมูลบทความ",
      dataMyblogs: paramMyblogs,
    });
  });
});

router.get("/delete/:id", function (req, res, next) {
  MyBlogs.DeleteMyBlogs([req.params.id], function (err) {
    if (err) throw err;
    console.log("DELETE COMPETED.", req.params.id);
    res.redirect("/myblog");
  });
});

router.get("/edit/:id", function (req, res, next) {
  MyBlogs.GetMyBlogsById([req.params.id], function (err, data) {
    if (err) throw err;
    res.render("blogs/addform", {
      header: "แก้ไขบทความ",
      mode: "edit",
      data: data,
    });
  });
});

router.get("/add", function (req, res, next) {
  var data = new MyBlogs({
    name: "Test",
    description: "Test_description",
    author: "Test_author",
  });

  res.render("blogs/addform", {
    header: "เพิ่มบทความ",
    mode: "add",
    data: data,
  });
});

router.post(
  "/add",
  [
    check("name", "Please Input your blog name!")
      .replace(" ", "")
      .not()
      .isEmpty(),
    check("description", "Please Input your blog description!")
      .replace(" ", "")
      .not()
      .isEmpty(),
    check("author", "Please Input your blog author name!")
      .replace(" ", "")
      .not()
      .isEmpty(),
  ],
  function (req, res, next) {
    //function ตอนกด Submit รู้จาก /views/addblog.ejs และต้องมีการเซ็ตค่า post จากform มาด้วย
    const result = validationResult(req);
    var errors = result.errors;
    if (!result.isEmpty()) {
      res.render("/myblog/add", { errors: errors }); // ทำการส่งError ไปหน้าแสดงผลที่หน้า/views/addblogs.ejs/ ที่ฟังก์ชั่นเช็ค  <% if(locals.errors) { %>
      //return res.status(400).json({ errors: errors.array() });
    } else {
      //insert into db
      var _name = req.body.name;
      var _description = req.body.description;
      var _author = req.body.author;
      data = new MyBlogs({
        name: _name,
        description: _description,
        author: _author,
      });

      MyBlogs.CreateMyBlogs(data, function (err, callback) {
        //if(err) console.log(err);
        if (err) {
          res.send(err);
        } else {
          req.flash("error", "บันทึกบทความเรียบร้อย");
          res.location("/myblog/add");
          res.redirect("/myblog/add");
        }
      });
    }
  }
);

router.post(
  "/edit",
  [
    check("name", "Please Input your blog name!")
      .replace(" ", "")
      .not()
      .isEmpty(),
    check("description", "Please Input your blog description!")
      .replace(" ", "")
      .not()
      .isEmpty(),
    check("author", "Please Input your blog author name!")
      .replace(" ", "")
      .not()
      .isEmpty(),
  ],
  function (req, res, next) {
    const result = validationResult(req);
    var errors = result.errors;
    if (!result.isEmpty()) {
      res.render("/myblog/add", { errors: errors }); // ทำการส่งError ไปหน้าแสดงผลที่หน้า/views/addblogs.ejs/ ที่ฟังก์ชั่นเช็ค  <% if(locals.errors) { %>
    } else {
      var _name = req.body.name;
      var _description = req.body.description;
      var _author = req.body.author;
      var _id = req.body.id;
      var data = new MyBlogs({
        name: _name,
        description: _description,
        author: _author,
        id: _id,
      });
      MyBlogs.UpdateMyBlogs(data, function (err, callback) {
        //if(err) console.log(err);
        if (err) {
          res.send(err);
        } else {
          req.flash("error", "แก้ไขบทความเรียบร้อย");
          res.location("/myblog/add");
          res.redirect("/myblog/add");
        }
      });
    }
  }
);

module.exports = router;
