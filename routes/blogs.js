var express = require("Express");
var router = express.Router();
const { check, validationResult } = require("express-validator"); //ทำการเช็คก่อนที่ส่งเข้าฐานข้อมูล

const monk = require("monk");
const url = "localhost:27017/TutorialDB";
const db = monk(url);

var flash = require("connect-flash");
var app = express();

router.get("/", function (req, res, next) {
  //function ตอนกดปุ่ม Nav รู้จาก /views/index.ejs
  res.render("blog");
});

router.get("/add", function (req, res, next) {
  //function ตอนกดลิ้งค์(เพิ่มบทความ) รู้จาก /views/blog.ejs
  res.render("addblog");
});
router.get("/myblog/delete/:id", function (req, res, next) {
  console.log("Find Id:", req.params.id);
});

router.post(
  "/add",
  [
    check("name", "Please Input your blog name!")
      .replace(" ", "")
      .not()
      .isEmpty(),
    check("description", "Please Input your blog description!")
      .replace(" ", "")
      .not()
      .isEmpty(),
    check("author", "Please Input your blog author name!")
      .replace(" ", "")
      .not()
      .isEmpty(),
  ],
  function (req, res, next) {
    //function ตอนกด Submit รู้จาก /views/addblog.ejs และต้องมีการเซ็ตค่า post จากform มาด้วย
    const result = validationResult(req);
    var errors = result.errors;
    if (!result.isEmpty()) {
      res.render("addblog", { errors: errors }); // ทำการส่งError ไปหน้าแสดงผลที่หน้า/views/addblogs.ejs/ ที่ฟังก์ชั่นเช็ค  <% if(locals.errors) { %>
      //return res.status(400).json({ errors: errors.array() });
    } else {
      //insert into db
      var ct = db.get("blogs");

      var _name = req.body.name;
      var _description = req.body.description;
      var _author = req.body.author;

      ct.insert(
        { name: _name, description: _description, author: _author },
        function (err, blog) {
          if (err) {
            res.send(err);
          } else {
            req.flash("error", "บันทึกบทความเรียบร้อย");
            res.location("/blog/add");
            res.redirect("/blog/add");
          }
        }
      );
    }

    /*
console.log(req.body.name);
console.log(req.body.description);
console.log(req.body.author);
*/
  }
);

module.exports = router;
