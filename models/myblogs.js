const mongoose = require("mongoose");
const mongo = require("mongodb");

const url = "mongodb://localhost:27017/TutorialDB";

mongoose.connect(url, {
  useNewUrlParser: true,
});

const db = mongoose.connection;
const Schema = mongoose.Schema;

const myblogSchema = new Schema({
  id: {
    type: Schema.ObjectId,
  },
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  author: {
    type: String,
    required: true,
  },
});

const MyblogsModel = (module.exports = mongoose.model(
  "blogs" /*ตรงนี้คือชื่อTable ใน mongodb*/,
  myblogSchema
));

//สร้างFunction สำหรับการบันทึกข้อมูล
module.exports.CreateMyBlogs = function (newMyBlogs, callback) {
  newMyBlogs.save(callback);
};

//สร้างFunction สำหรับการแสดงผล
module.exports.GetAllMyBlogs = function (data) {
  MyblogsModel.find(data);
};

//สร้างFunction สำหรับลบข้อมูล
module.exports.DeleteMyBlogs = function (id, callback) {
  MyblogsModel.findByIdAndDelete(id, callback);
};
//สร้างFunction สำหรับแก้ไขข้อมูล
module.exports.UpdateMyBlogs = function (data, callback) {
  var qry = {
    _id: data.id,
  };
  MyblogsModel.findByIdAndUpdate(qry,{
    $set:{
      name:data.name,
      description:data.description,
      author:data.author
    }
  },{new:true}, callback);
};
//สร้างFunction สำหรับ Get ข้อมูล
module.exports.GetMyBlogsById = function (id, callback) {
  var qry = {
    _id: id,
  };
  MyblogsModel.findOne(qry, callback);
};
